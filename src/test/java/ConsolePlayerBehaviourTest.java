import org.junit.Test;

import java.util.Scanner;

import static org.junit.Assert.*;

public class ConsolePlayerBehaviourTest {

    @Test
    public void shouldMakeCHEATMove() {
        Scanner in = new Scanner("cheat");
        PlayerBehaviour playerBehaviour = new ConsolePlayerBehaviour(in);
        MoveType move = playerBehaviour.makeMove();
        assertEquals(MoveType.CHEAT, move);
    }

    @Test
    public void shouldMoveCOOPERATEMove() {
        Scanner in = new Scanner("COOPERATE");
        PlayerBehaviour playerBehaviour = new ConsolePlayerBehaviour(in);
        MoveType move = playerBehaviour.makeMove();
        assertEquals(MoveType.COOPERATE, move);
    }


    @Test
    public void shouldThrowErrorOnInvalidMove() {
        Scanner in = new Scanner("INVALID ENTRY");
        PlayerBehaviour playerBehaviour = new ConsolePlayerBehaviour(in);
        MoveType move = playerBehaviour.makeMove();
        assertEquals(MoveType.INVALID_MOVE, move);
    }


}