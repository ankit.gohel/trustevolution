import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CoolPlayerBehaviourTest {
    CoolPlayerBehaviour coolPlayer = new CoolPlayerBehaviour();

    @Test
    public void shouldReturnCOOPERATEMoveType() {
        assertEquals(MoveType.COOPERATE, coolPlayer.makeMove());
    }

    @Test
    public void shouldNotReturnAnyOtherMoveType() {
        assertNotEquals(MoveType.INVALID_MOVE, coolPlayer.makeMove());
    }

}