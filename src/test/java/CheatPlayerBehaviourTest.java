import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class CheatPlayerBehaviourTest {

    CheatPlayerBehaviour cheater = new CheatPlayerBehaviour();

    @Test
    public void shouldReturnCHEATMoveType() {
        assertEquals(MoveType.CHEAT, cheater.makeMove());
    }

    @Test
    public void shouldNotReturnAnyOtherMoveType() {
        assertNotEquals(MoveType.INVALID_MOVE, cheater.makeMove());
    }

}
