import org.junit.Test;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class PlayerTest {
    @Test
    public void shouldUpdateScore() {
        Scanner in = new Scanner("COOPERATE");
        PlayerBehaviour playerBehaviour = new ConsolePlayerBehaviour(in);
        Player player = new Player(playerBehaviour);
        player.updateScore(4);
        assertEquals(4, player.getScore());
    }

}
