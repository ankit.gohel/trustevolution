import java.util.Objects;

public class Score {

    private int player1Score;
    private int player2Score;

    public Score(int player1Score, int player2Score) {
        this.player1Score = player1Score;
        this.player2Score = player2Score;
    }

    public int getPlayer1Score() {
        return player1Score;
    }

    public int getPlayer2Score() {
        return player2Score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Score)) return false;
        Score score = (Score) o;
        return getPlayer1Score() == score.getPlayer1Score() &&
                getPlayer2Score() == score.getPlayer2Score();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPlayer1Score(), getPlayer2Score());
    }

    @Override
    public String toString() {
        return "Player 1 Score: "+player1Score+"\n"+"Player 2 Score: "+player2Score;
    }
}
