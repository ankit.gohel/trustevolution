public interface PlayerBehaviour {

    MoveType makeMove();
}
