public enum MoveType {
    COOPERATE, CHEAT, INVALID_MOVE;

    public static MoveType getMoveTypeFromInput(String input){
        return MoveType.valueOf(input.toUpperCase());
    }
}
