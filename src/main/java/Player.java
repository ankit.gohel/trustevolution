public class Player {

    private PlayerBehaviour playerBehaviour;
    private int score;

    public Player(PlayerBehaviour playerBehaviour) {
        score=0;
        this.playerBehaviour = playerBehaviour;
    }

    public MoveType selectMove() {
       return playerBehaviour.makeMove();
    }

    public void updateScore(int score){
        this.score += score;
    }
    public int getScore(){ return score; }
}