import java.util.Scanner;

public class ConsolePlayerBehaviour implements PlayerBehaviour {

    private Scanner scanner;

    public ConsolePlayerBehaviour(Scanner scanner) {
        this.scanner = scanner;
    }

    @Override
    public MoveType makeMove() {
        String playerChoice = scanner.next();
        try {
            if (playerChoice != null || playerChoice.length() <= 0) {
                MoveType move = MoveType.valueOf(playerChoice.toUpperCase());
                return move;
            }
        }
        catch(Exception e){
            return MoveType.INVALID_MOVE;
        }
        return null;
    }
}
